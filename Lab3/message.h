#ifndef LAB3_MESSAGE_H
#define LAB3_MESSAGE_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <limits.h>
#include <stdbool.h>

#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

#define DATA_MAX 3000

#define HELLO_MSG_TXT   "hello from hell"

#define HELP_MSG_TXT    "help       - shows this help message\n"                \
                        "pwd        - prints current directory\n"               \
                        "cd         - change directory\n"                       \
                        "ls         - show all files in current directory\n"    \
                        "cat        - prints file content\n"                    \
                        "upload     - uploads file on server\n"                 \
                        "download   - downloads file from server\n"             \
                        "exit       - stops the program\n"

enum command {
    EOSIG = 0,
    HELP,
    PWD,
    CD,
    LS,
    CAT,
    UPLOAD,
    DOWNLOAD,
    EXIT,
    HELLO, // handshake with server
    ERROR, // server error. only from server
    UPDATE // update current directory
};

struct constructed_message {
    enum command command;
    char dir[PATH_MAX];
    unsigned long long data_length;
    char* data;
};

void send_constructed_message(struct constructed_message* constructed_message, int socket_desc);
struct constructed_message* receive_constructed_message(int socket_desc);
struct constructed_message* copy_constructed_message(struct constructed_message* src);
void free_constructed_message(struct constructed_message* message);


#endif //LAB3_MESSAGE_H

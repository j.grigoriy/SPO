#include "opts.h"

static char* optstr = "hcsa:p:d:";

void print_help() {
    puts("Lab 3 SPO by Grigoriy Javoronkov");
    puts("Usage:");
    puts("\t-h\tPrints this help message");
    puts("\t-s\tStarts program in server mode. In this mode you also need to specify -d and -p parameter");
    puts("\t\t-d\tSpecifies root directory for your server");
    puts("\t-c\tStarts program in client mode. In this mode you also need to specify -a and -p parameters");
    puts("\t\t-a\tSpecifies ip address of server");
    puts("\t-p\tSpecifies port of server");
}

void get_options(int argc, char** argv, struct options* options) {
    char client_flag = 0;
    char server_flag = 0;
    int c;
    options->mode = UNDEFINED;
    while ((c = getopt(argc, argv, optstr)) != -1) {
        switch (c) {
            case 'c': client_flag = 1; options->mode = CLIENT; break;
            case 's': server_flag = 1; options->mode = SERVER; break;
            case 'a': strcpy(options->client_params.server_ip, optarg); break;
            case 'p': options->server_port = strtol(optarg, NULL, 10); break;
            case 'd': strcpy(options->server_params.root_path, optarg); break;
            default: print_help(); exit(666);
        }
    }
    if (client_flag && server_flag || options->mode == UNDEFINED) {
        print_help();
        exit(666);
    }
}
#include "utils.h"

static struct operation operations[] = {
        { LS,   "ls",   "Output files with attributes in directory" },
        { COPY, "cp",   "Copy files or directories" },
        { PWD,  "pwd",  "Show currect directory" },
        { CD,   "cd",   "Change directory" },
        { HELP, "h",    "Prints list of available commands" },
        { QUIT, "q",    "Exit the program" }
};

struct operation* get_operations() {
    return operations;
}

size_t get_operations_size() {
    return sizeof(operations) / sizeof(struct operation);
}

void execute_xfs_operation(enum command command_type, char* output_buf, int argc, char** argv, struct xfs_state* xfs_state) {
    switch (command_type) {
        case LS:
            xfs_ls(output_buf, xfs_state);
            break;
        case COPY:
            if (argc == 2) {
                xfs_copy(output_buf, argv[0], argv[1], xfs_state);
            }
            break;
        case PWD:
            xfs_pwd(output_buf, xfs_state);
            break;
        case CD:
            if (argc == 1) {
                xfs_cd(output_buf, argv[0], xfs_state);
            }
            break;
        case QUIT:
            break;
        default:
            execute_help(output_buf);
    }
}

int str_starts_with(char* src, char* substr) {
    if (strncmp(src, substr, strlen(substr)) == 0) {
        return 1;
    } else {
        return 0;
    }
}

void get_disks_and_partitions(char* output_buf) {
    DIR* sys_block_dir = opendir(SYS_BLOCK_DIR);
    DIR* sys_block_child_dir;
    struct dirent* sys_block_entry;
    struct dirent* sys_block_child_entry;
    char block_filename[PATH_MAX] = {'\0'};

    if (sys_block_dir != NULL) {
        while ((sys_block_entry = readdir(sys_block_dir)) != NULL) {
            if (str_starts_with(sys_block_entry->d_name, "sd")) {
                strcat(output_buf, "/dev/");
                strcat(output_buf, sys_block_entry->d_name);
                strcat(output_buf, "\t\tdisk\n");
                strcpy(block_filename, SYS_BLOCK_DIR);
                strcat(block_filename, sys_block_entry->d_name);
                sys_block_child_dir = opendir(block_filename);
                if (sys_block_child_dir != NULL) {
                    while ((sys_block_child_entry = readdir(sys_block_child_dir)) != NULL) {
                        if (str_starts_with(sys_block_child_entry->d_name, "sd")) {
                            strcat(output_buf, "/dev/");
                            strcat(output_buf, sys_block_child_entry->d_name);
                            strcat(output_buf, "\t\tpartition\n");
                        }
                    }
                } else {
                    strcat(output_buf, "Can't read from ");
                    strcat(output_buf, sys_block_entry->d_name);
                    strcat(output_buf, "\n");
                }
            }
        }
    } else {
        strcpy(output_buf, "Can't read from ");
        strcat(output_buf, SYS_BLOCK_DIR);
    }
}
#ifndef OPTS_H_
#define OPTS_H_

enum program_mode {
    UNDEFINED = 0,
    ALL_FILESYSTEMS = 1,
    XFS_ONLY = 2
};

struct program_args {
    enum program_mode mode;
    char* start_path;
};

struct program_args get_program_args(int argc, char** argv);
void show_usage();

#endif
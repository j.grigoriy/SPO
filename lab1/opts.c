#include "opts.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <getopt.h>
#include <string.h>

const char* optString = "m:p:";

struct program_args get_program_args(const int argc, char** argv) {
    struct program_args args;
    int opt;

    args.mode = UNDEFINED;
    args.start_path = NULL;

    while ((opt = getopt(argc, argv, optString)) != -1) {
        switch (opt) {
            case 'm':
                if (strcmp(optarg, "1") == 0) {
                    args.mode = ALL_FILESYSTEMS;
                } else if (strcmp(optarg, "2") == 0) {
                    args.mode = XFS_ONLY;
                }
                break;
            case 'p':
                args.start_path = optarg;
                break;
            case '?':
            default:
                break;
        }
    }

    return args;
}

void show_usage() {
    puts("spo1 - program to work with filesystem");
    puts("keys:");
    puts("\t-m - program mode. 1 - work with all filesystems, 2 - work only with xft");
    puts("\t-p - start path. In mode 1 - could be any possible path. In mode 2 - device name (/dev/sd*)");
    exit(0);
}
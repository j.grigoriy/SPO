#include "opts.h"
#include "ui.h"

int main(int argc, char** argv) {
    struct program_args args = get_program_args(argc, argv);
    if (args.mode == UNDEFINED) {
        show_usage();
    } else {
        start_ui(args.mode, args.start_path);
    }
    return 0;
}
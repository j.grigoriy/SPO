#include "xfs_operations.h"

static inline __uint64_t xfs_mask64lo(int n) {
    return ((__uint64_t) 1 << (n)) - 1;
}

static inline xfs_uint8_t* xfs_dir2_sf_ftype(struct xfs_dir2_sf_entry* entry) {
    return (entry->name + entry->namelen);
}

static inline long inou_t_to_long(xfs_dir2_inou_t* inou) {
    return inou->i4.i[0] * 255 * 255 * 255 + inou->i4.i[1] * 255 * 255 + inou->i4.i[2] * 255 + inou->i4.i[3];
}

static inline long xfs_dir2_sf_inumber(struct xfs_dir2_sf_entry* entry) {
    return inou_t_to_long((xfs_dir2_inou_t *) (entry->name + entry->namelen + sizeof(entry->ftype)));
}

static inline long xfs_dir2_sf_parent(struct xfs_dir2_sf_hdr* hdr) {
    return inou_t_to_long(&hdr->parent);
}

static inline unsigned long xfs_dir2_sf_entry_true_size(struct xfs_dir2_sf_entry* entry) {
    return sizeof(struct xfs_dir2_sf_entry) + entry->namelen - sizeof(xfs_dir2_ino4_t) - 1;
}

static inline unsigned long xfs_dir2_sf_entry_address_offset() {
    return sizeof(struct xfs_dinode_core) + sizeof(struct xfs_dir2_sf_hdr) - (sizeof(xfs_dir2_ino8_t) - sizeof(xfs_dir2_ino4_t));
}

void xfs_dir2_sf_filename(struct xfs_dir2_sf_entry* entry, char* filename) {
    for (xfs_uint8_t i = 0; i < entry->namelen; i++) {
        filename[i] = entry->name[i];
    }
    if (*xfs_dir2_sf_ftype(entry) == 2) {
        filename[entry->namelen] = '/';
        filename[entry->namelen + 1] = '\0';
    } else {
        filename[entry->namelen] = '\0';
    }
}

void construct_path(char* path, char* result) {
    char* token;
    unsigned long current_path_len;
    char* path_copy;

    if (path[0] == '/') {
        result[1] = '\0';
    }
    path_copy = calloc(strlen(path) + 1, sizeof(char));
    strcpy(path_copy, path);
    token = strtok(path_copy, "/");

    while (token != NULL) {
        strcat(result, token);
        current_path_len = strlen(result);
        if (strcmp(token, ".") == 0) {
            result[current_path_len - 1] = '\0';
            result[current_path_len] = '\0';
        } else if (strcmp(token, "..") == 0) {
            current_path_len -= 4;
            while (result[current_path_len] != '/') current_path_len--;
            result[++current_path_len] = '\0';
        } else {
            strcat(result, "/");
        }
        token = strtok(NULL, "/");
    }

    if (strcmp(path_copy, "/..") == 0) {
        path[1] = '\0';
    }

    free(path_copy);
}

void split_path(char* path, char* dir) {
    unsigned long path_len = strlen(path);
    if (path[--path_len] == '/') path[path_len] = '\0';
    while (path[--path_len] != '/' && path_len >= 0);
    path[path_len+1] = '\0';
    if (path_len == 0) {
        dir = NULL;
    } else {
        dir = path;
    }
}

void read_sb(struct xfs_state* xfs_state) {
    if (xfs_state->device_pointer == NULL) {
        xfs_state->error = CANT_READ_SB;
        return;
    }
    fread(&xfs_state->sb, sizeof(struct xfs_sb), 1, xfs_state->device_pointer);
    xfs_state->sb.sb_magicnum = bswap_32(xfs_state->sb.sb_magicnum);
    xfs_state->sb.sb_blocksize = bswap_32(xfs_state->sb.sb_blocksize);
    xfs_state->sb.sb_rootino = bswap_64(xfs_state->sb.sb_rootino);
    xfs_state->sb.sb_inodesize = bswap_16(xfs_state->sb.sb_inodesize);
    if (xfs_state->sb.sb_magicnum != XFS_SB_MAGIC) {
        xfs_state->error = UNSUPPORTED_FILESYSTEM;
    }
}

void read_dinode(struct xfs_state* xfs_state, struct xfs_dinode* dinode) {
    fseek(xfs_state->device_pointer, xfs_state->address, SEEK_SET);
    fread(dinode, sizeof(struct xfs_dinode), 1, xfs_state->device_pointer);
    dinode->di_core.di_magic = bswap_16(dinode->di_core.di_magic);
    dinode->di_core.di_ino = bswap_64(dinode->di_core.di_ino);
    dinode->di_core.di_nextents = bswap_32(dinode->di_core.di_nextents);
    dinode->di_core.di_nblocks = bswap_64(dinode->di_core.di_nblocks);
    dinode->di_core.di_size = bswap_64(dinode->di_core.di_size);
    if (dinode->di_core.di_magic != XFS_DINODE_MAGIC) {
        xfs_state->error = CANT_READ_DINODE;
    }
}

struct xfs_state* init(char* device_path, struct xfs_state* xfs_state) {
    if (xfs_state == NULL) {
        xfs_state = malloc(sizeof(struct xfs_state));
    }
    xfs_state->error = OK;
    strcpy(xfs_state->path, device_path);
    xfs_state->device_pointer = fopen(device_path, "rb");
    read_sb(xfs_state);
    if (xfs_state->error) {
        return xfs_state;
    }
    xfs_state->address = xfs_state->sb.sb_rootino * xfs_state->sb.sb_inodesize;
    read_dinode(xfs_state, &xfs_state->dinode);
    xfs_state->path[0] = '/';
    xfs_state->path[1] = '\0';
    return xfs_state;
}

void destroy(struct xfs_state* xfs_state) {
    fclose(xfs_state->device_pointer);
    free(xfs_state);
}

void xfs_readdir(char* output_buf, struct xfs_state* xfs_state) {
    char filename[FILENAME_MAX+2] = {'\0'};
    unsigned long long address = xfs_state->address;
    xfs_dir2_sf_entry_t* entry = malloc(sizeof(struct xfs_dir2_sf_entry) + FILENAME_MAX);
    output_buf[0] = '\0';
    address += xfs_dir2_sf_entry_address_offset();
    fseek(xfs_state->device_pointer, address, SEEK_SET);

    for (unsigned char i = 0; i < xfs_state->dinode.di_u.di_dir2sf.hdr.count; i++) {
        fread(entry, sizeof(struct xfs_dir2_sf_entry) + FILENAME_MAX, 1, xfs_state->device_pointer);
        xfs_dir2_sf_filename(entry, filename);
        strcat(output_buf, filename);
        strcat(output_buf, "\n");
        address += xfs_dir2_sf_entry_true_size(entry);
        fseek(xfs_state->device_pointer, address, SEEK_SET);
    }
    free(entry);
}

static long find_inode(char* name, int ftype, struct xfs_state* xfs_state) {
    char filename[FILENAME_MAX+2] = {'\0'};
    unsigned long long address = xfs_state->address;
    xfs_dir2_sf_entry_t* entry = malloc(sizeof(struct xfs_dir2_sf_entry) + FILENAME_MAX);

    if (ftype == 2) {
        if (strcmp(name, ".") == 0) {
            return xfs_state->dinode.di_core.di_ino;
        } else if (strcmp(name, "..") == 0) {
            return xfs_dir2_sf_parent(&xfs_state->dinode.di_u.di_dir2sf.hdr);
        }
    }

    address += xfs_dir2_sf_entry_address_offset();
    fseek(xfs_state->device_pointer, address, SEEK_SET);
    for (unsigned char i = 0; i < xfs_state->dinode.di_u.di_dir2sf.hdr.count; i++) {
        fread(entry, sizeof(struct xfs_dir2_sf_entry) + FILENAME_MAX, 1, xfs_state->device_pointer);
        xfs_dir2_sf_filename(entry, filename);
        if (ftype == 2)
            filename[entry->namelen] = '\0';
        if (*xfs_dir2_sf_ftype(entry) == ftype && strcmp(filename, name) == 0) {
            long inode = xfs_dir2_sf_inumber(entry);
            free(entry);
            return inode;
        }
        address += xfs_dir2_sf_entry_true_size(entry);
        fseek(xfs_state->device_pointer, address, SEEK_SET);
    }
    free(entry);
    return -1;
}

static long find_dir_inode(char* dir, struct xfs_state* xfs_state) {
    return find_inode(dir, 2, xfs_state);
}

static long find_file_inode(char* filename, struct xfs_state* xfs_state) {
    return find_inode(filename, 1, xfs_state);
}

void read_file_data(char* filename, char* filedata, struct xfs_state* xfs_state) {
    struct xfs_bmbt_irec bmbt;
    struct xfs_dinode dinode;
    unsigned long long saved_address = xfs_state->address;
    long finode = find_file_inode(filename, xfs_state);
    size_t _filedata_size = 0;
    char* _filedata = calloc(TMP_MAX, sizeof(char));
    char input_char;

    xfs_state->address = finode * xfs_state->sb.sb_inodesize;
    read_dinode(xfs_state, &dinode);
    xfs_state->address = saved_address;
    xfs_uint64_t l0 = bswap_64(dinode.di_u.di_bmx->l0);
    xfs_uint64_t l1 = bswap_64(dinode.di_u.di_bmx->l1);
    swap(l0, l1);
    bmbt.br_state = XFS_EXT_NORM;
    int ext_flag = (l0 >> (64 - BMBT_EXNTFLAG_BITLEN));
    bmbt.br_startoff = ((xfs_fileoff_t)l0 &
                        xfs_mask64lo(64 - BMBT_EXNTFLAG_BITLEN)) >> 9; // EXACT (no * sb inodesize) address of data
    bmbt.br_startblock = (((xfs_fsblock_t)l0 & xfs_mask64lo(9)) << 43) |
                         (((xfs_fsblock_t)l1) >> 21);
    bmbt.br_blockcount = (xfs_filblks_t)(l1 & xfs_mask64lo(21));

    if (bmbt.br_startoff == 0) {
        free(_filedata);
        return;
    }
    fseek(xfs_state->device_pointer, bmbt.br_startoff, SEEK_SET);
    while ((input_char = fgetc(xfs_state->device_pointer)) != '\0') {
        _filedata[_filedata_size++] = input_char;
    }
    _filedata[_filedata_size] = '\0';

    if (filedata == NULL) {
        filedata = _filedata;
    } else {
        strcpy(filedata, _filedata);
        free(_filedata);
    }
}

void dir_copy(char* output_buf, char* to, struct xfs_state* xfs_state) {
    char files_list[TMP_MAX] = {'\0'};
    char buffer[TMP_MAX] = {'\0'};
    char out_buf[PATH_MAX*2 + 20] = {'\0'};
    char buffer2[TMP_MAX] = {'\0'};

    xfs_readdir(files_list, xfs_state);
    char* token = strtok(files_list, "\n");
    while (token != NULL) {
        strcpy(buffer2, token);
        if (buffer2[strlen(buffer2) - 1] == '/') {
            xfs_cd(buffer, buffer2, xfs_state);
            strcat(to, buffer2);
            mkdir(to, ACCESSPERMS);
            token += strlen(token) + 1;
            dir_copy(output_buf, to, xfs_state);
            xfs_cd(buffer, "..", xfs_state);
            split_path(to, to);
            buffer[0] = '\0';
            token = strtok(token, "\n");
        } else {
            read_file_data(buffer2, buffer, xfs_state);
            strcat(to, buffer2);
            FILE* fp = fopen(to, "w");
            fwrite(buffer, strlen(buffer), 1, fp);
            fclose(fp);
            snprintf(out_buf, PATH_MAX*2 + 20, "Extracting %s%s to %s\n", xfs_state->path, buffer2, to);
            split_path(to, NULL);
            strcat(output_buf, out_buf);
            buffer[0] = '\0';
            token = strtok(NULL, "\n");
        }
    }
}

void xfs_ls(char* output_buf, struct xfs_state* xfs_state) {
    xfs_readdir(output_buf, xfs_state);
}

void xfs_copy(char* output_buf, char* from, char* to, struct xfs_state* xfs_state) {
    char user_path[PATH_MAX];

    strcpy(user_path, xfs_state->path);
    xfs_cd(output_buf, from, xfs_state);
    if (output_buf[0] != '\0') {
        return;
    }
    dir_copy(output_buf, to, xfs_state);
    xfs_cd(output_buf, user_path, xfs_state);
}

void xfs_pwd(char* output_buf, struct xfs_state* xfs_state) {
    strcpy(output_buf, xfs_state->path);
    strcat(output_buf, "\n");
}

void xfs_cd(char* output_buf, char* path, struct xfs_state* xfs_state) {
    struct xfs_dinode saved_dinode = xfs_state->dinode;
    unsigned long long saved_address = xfs_state->address;
    unsigned long long inode;
    char* tempstr = calloc(strlen(path) + 1, sizeof(char));
    strcpy(tempstr, path);
    char* token = strtok(tempstr, "/");
    if (path[0] == '/') {
        // clear path and init root dinode
        xfs_state->path[0] = '/';
        xfs_state->path[1] = '\0';
        xfs_state->address = xfs_state->sb.sb_rootino * xfs_state->sb.sb_inodesize;
        read_dinode(xfs_state, &xfs_state->dinode);
    }
    while (token != NULL) {
        inode = find_dir_inode(token, xfs_state);
        if (inode == -1) {
            xfs_state->dinode = saved_dinode;
            xfs_state->address = saved_address;
            strcpy(output_buf, "Unknown directory\n");
            free(tempstr);
            return;
        }
        xfs_state->address = inode * xfs_state->sb.sb_inodesize;
        read_dinode(xfs_state, &xfs_state->dinode);
        token = strtok(NULL, "/");
    }
    construct_path(path, xfs_state->path);
    free(tempstr);
}

void execute_help(char* output_buf) {
    struct operation* operations = get_operations();
    size_t operations_size = get_operations_size();
    output_buf[0] = '\0';
    for (size_t i = 0; i < operations_size; i++) {
        strcat(output_buf, operations[i].name);
        strcat(output_buf, "\t");
        strcat(output_buf, operations[i].description);
        strcat(output_buf, "\n");
    }
}
#include "ui.h"


void print_welcome_screen(enum program_mode mode) {
    printf("**************************************\n");
    printf("*** Lab 1 SPO\n");
    printf("*** Made by Grigoriy Javoronkov\n");
    printf("*** Program mode: %d\n", mode);
    printf("**************************************\n");
}

void read_user_input(char* input) {
    input[0] = '\0';
    do {
        printf("> ");
        fgets(input, MAX_INPUT_LENGTH, stdin);
    } while (strlen(input) == 1);
    input[strlen(input) - 1] = '\0';
}

void execute_command_and_print_output(struct operation* operations, char* input, struct xfs_state* xfs_state) {
    int argc = 0;
    char* argv[MAX_OPERANDS] = { NULL };
    char input_copy[MAX_INPUT_LENGTH];
    char output_buf[TMP_MAX] = {'\0'};
    enum command selected_command = HELP;
    size_t operations_size = get_operations_size();
    strcpy(input_copy, input);
    char* input_ptr = strtok(input_copy, " ");
    for (size_t i = 0; i < operations_size; i++) {
        if (strcmp(operations[i].name, input_ptr) == 0) {
            selected_command = operations[i].command_type;
            break;
        }
    }
    while ((input_ptr = strtok(NULL, " ")) != NULL) {
        argv[argc] = malloc((strlen(input_ptr) + 1) * sizeof(char));
        strcpy(argv[argc], input_ptr);
        argc += 1;
    }
    execute_xfs_operation(selected_command, output_buf, argc, argv, xfs_state);
    printf("%s", output_buf);
    for (int i = 0; i < argc; i++) {
        free(argv[i]);
    }
}

void start_ui(enum program_mode mode, char* start_path) {
    struct operation* operations = get_operations();
    char input[MAX_INPUT_LENGTH] = {'\0'};
    char output_buf[TMP_MAX];
    struct xfs_state* xfs_state = NULL;

    print_welcome_screen(mode);
    if (mode == ALL_FILESYSTEMS) {
        get_disks_and_partitions(output_buf);
        printf("%s", output_buf);
        return;
    } else if (mode == XFS_ONLY) {
        xfs_state = init(start_path, NULL);
    }

    do {
        read_user_input(input);
        execute_command_and_print_output(operations, input, xfs_state);
    } while (strcmp(input, "q") != 0);


}
#ifndef UI_H_
#define UI_H_

#include <stdio.h>
#include <string.h>
#include <limits.h>
#include "opts.h"
#include "utils.h"
#include "xfs_operations.h"

#define MAX_INPUT_LENGTH 256
#define MAX_OPERANDS 100

void start_ui(enum program_mode mode, char* start_path);


#endif
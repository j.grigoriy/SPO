require_relative 'utils_module'

require 'tty-screen'
require 'fiddle'

include UtilsModule

module UI_module

  def printWelcomeScreen(mode)
    cols = TTY::Screen.cols
    welcomeStr1 = "* Lab 2 SPO. Written using ruby"
    welcomeStr2 = "* Made by: Javoronkov Grigoriy"
    welcomeStr3 = "* Program mode: " + mode.to_s

    puts
    puts("*" * cols)

    print(welcomeStr1)
    print(" " * (cols - welcomeStr1.length - 1))
    puts("*")

    print(welcomeStr2)
    print(" " * (cols - welcomeStr2.length - 1))
    puts("*")

    print(welcomeStr3)
    print(" " * (cols - welcomeStr3.length - 1))
    puts("*")

    puts("*" * cols)
  end

  def pointer_to_string(ffi_pointer)
    address = ffi_pointer.to_i
    fiddle = ::Fiddle::Pointer.new(address)
    fiddle.to_s
  end



  def startUI(mode, path)
    printWelcomeScreen(mode)

    if mode == 1
      output = UtilsModule.init_output_buffer
      UtilsModule.get_disks_and_partitions(output)
      str = pointer_to_string(output)
      print(str)
      UtilsModule.destroy_output_buffer(output)
    else
      output = UtilsModule.init_output_buffer
      xfs_struct = UtilsModule.xfs_init(path, nil, output)
      str = pointer_to_string(output)
      if str != ""
        print(str)
        return
      end
      begin
        input = ""
        while input != "q"
          print("> ")
          input = STDIN.gets.chomp
          if input != "" && input != "q"
            tokens = input.split(' ')
            command = UtilsModule.get_command_by_str(tokens[0])
            argc = tokens.length - 1
            argv = UtilsModule.init_argv(argc)
            for i in 0..argc-1
              UtilsModule.add_argv(i, argv, tokens[i+1])
            end
            output = UtilsModule.init_output_buffer
            UtilsModule.execute_xfs_operation(command, output, argc, argv, xfs_struct)
            str = pointer_to_string(output)
            print(str)
            UtilsModule.destroy_output_buffer(output)
            UtilsModule.destroy_argv(argv, argc)
          end
        end
      rescue Exception
        puts("Unexpected failure. Finishing...")
        return
      end
    end


  end
end
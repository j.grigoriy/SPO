require 'getopt/long'

module OPTS_Module

  def getOpts
    params = {}
    begin
      opt = Getopt::Long.getopts(
        ["--mode", "-m", Getopt::REQUIRED],
        ["--path", "-p", Getopt::OPTIONAL],
        ["--help", "-h", Getopt::BOOLEAN]
      )
      if opt["mode"]
        params["mode"] = opt["mode"].to_i
      end
      if opt["path"]
        params["path"] = opt["path"]
      end
      if opt["help"]
        params["help"] = true
      else
        params["help"] = false
      end

      return params
    rescue Exception
      return nil
    end
  end

  def validateParams(params)
    if !params ||
       params["help"] ||
      !params["mode"] ||
       params["mode"] != 1 &&
       (params["mode"] == 2 && !params["path"])

      return false
    end

    true
  end

  def printHelp
    puts("This works in 2 mods. First one - shows all disks and partitions. Second - work with xfs filesystem.")
    puts("Program arguments:")
    puts("\t--mode\t-m\tMode selection. Required argument.")
    puts("\t--path\t-p\tPath to block device. Required in second mode.")
    puts("\t--help\t-h\tPrints this help.")
  end
end
require_relative 'opts_module'
require_relative 'ui_module'

include OPTS_Module
include UI_module

params = getOpts

if validateParams(params)
  startUI(params["mode"], params["path"])
else
  printHelp
end